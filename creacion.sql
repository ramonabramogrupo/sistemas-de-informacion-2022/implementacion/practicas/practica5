﻿DROP DATABASE IF EXISTS practica5; 
CREATE DATABASE practica5;
USE practica5; 

CREATE TABLE alumno(
  expediente int,
  nombre varchar(100),
  apellidos varchar(200),
  fecha_Nac date
  );

CREATE TABLE esDelegado(
  `expediente-delegado` int,
  `expediente-alumno` int
  );

CREATE TABLE cursa(
  `expediente-alumno` int,
  `codigo-modulo` int
  );

CREATE TABLE modulo(
  codigo int,
  nombre varchar(200)
  );

CREATE TABLE imparte(
  `dni-profesor` varchar(10),
  `codigo-modulo` int
  );

CREATE TABLE profesor(
  dni varchar(10),
  nombre varchar(200),
  direccion varchar(500),
  tfno varchar(15)
  );

ALTER TABLE alumno 
  ADD PRIMARY KEY(expediente),
  MODIFY expediente int AUTO_INCREMENT;

ALTER TABLE esDelegado
  ADD PRIMARY KEY (`expediente-delegado`,`expediente-alumno`),
  ADD UNIQUE KEY uk1 (`expediente-alumno`);

ALTER TABLE cursa
  ADD PRIMARY KEY (`expediente-alumno`,`codigo-modulo`);

ALTER TABLE modulo
  ADD PRIMARY KEY (codigo);

ALTER TABLE imparte 
  ADD PRIMARY KEY (`dni-profesor`,`codigo-modulo`);

ALTER TABLE profesor
  ADD PRIMARY KEY (dni);

ALTER TABLE esDelegado 
   ADD CONSTRAINT fkesDelegadoAlumno1 
      FOREIGN KEY (`expediente-delegado`)
        REFERENCES alumno(expediente)
        ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkesDelegadoAlumno2 
      FOREIGN KEY (`expediente-alumno`)
        REFERENCES alumno(expediente)
        ON DELETE RESTRICT ON UPDATE RESTRICT;
  
  ALTER TABLE cursa
    ADD CONSTRAINT fkCursaAlumno
      FOREIGN KEY (`expediente-alumno`)
      REFERENCES alumno(expediente)
      ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD CONSTRAINT fkCursaModulo
      FOREIGN KEY (`codigo-modulo`)
      REFERENCES modulo(codigo)
      ON DELETE RESTRICT ON UPDATE RESTRICT;

  ALTER TABLE imparte 
    ADD CONSTRAINT fkImparteProfesor
      FOREIGN KEY (`dni-profesor`)
      REFERENCES profesor(dni)
      ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD CONSTRAINT fkImparteModulo 
      FOREIGN KEY (`codigo-modulo`)
      REFERENCES modulo(codigo)
      ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD UNIQUE KEY (`codigo-modulo`);




