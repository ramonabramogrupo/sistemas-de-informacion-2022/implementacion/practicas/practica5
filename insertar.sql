﻿USE practica5;
INSERT INTO alumno 
  (expediente, nombre, apellidos, fecha_Nac) VALUES 
  (1, 'raul', 'gomez', NULL),
  (2,'ana','lopez',NULL),
  (3,'luisa','vidal',NULL);

INSERT INTO esdelegado 
  (`expediente-delegado`, `expediente-alumno`) VALUES 
  (1, 2),
  (1,3);

INSERT INTO modulo 
  (codigo, nombre) VALUES 
  (1, 'Excel'),
  (2, 'Access'),
  (3, 'Word');

INSERT INTO cursa 
  (`expediente-alumno`, `codigo-modulo`) VALUES 
  (1, 1),
  (1,2),
  (1,3),
  (2,3);

INSERT INTO profesor 
  (dni, nombre, direccion, tfno) VALUES 
  ('d1', 'daniel', NULL, NULL),
  ('p2','lorena',NULL,NULL);

INSERT INTO imparte 
  (`dni-profesor`, `codigo-modulo`) VALUES 
  ('d1',1),
  ('d1',2),
  ('p2',3);
  